﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestWorkGame
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainMenu 
    {
        
        public MainMenu()
        {
            InitializeComponent();
        }
        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        private void StartGameButton_Click(object sender, RoutedEventArgs e)
        {
            GameWindow window = new GameWindow();
            Close();
            window.Show();

        }
        private void GameRulesButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("В классическом варианте игра рассчитана на двух игроков. Каждый из игроков задумывает и записывает тайное 4-значное число с неповторяющимися цифрами. Игрок, который начинает игру по жребию, делает первую попытку отгадать число. Попытка — это 4-значное число с неповторяющимися цифрами, сообщаемое противнику. Противник сообщает в ответ, сколько цифр угадано без совпадения с их позициями в тайном числе (то есть количество коров) и сколько угадано вплоть до позиции в тайном числе (то есть количество быков).Например"
                + Environment.NewLine + "Задумано тайное число «3219»."
                + Environment.NewLine + "Попытка: «2310»."
                + Environment.NewLine + "Результат: две «коровы» (две цифры: «2» и «3» — угаданы на неверных позициях) и один «бык» (одна цифра «1» угадана вплоть до позиции)."
                + Environment.NewLine + "При игре против компьютера игрок вводит комбинации одну за другой, пока не отгадает всю последовательность.","Правила игры",MessageBoxButton.OK);
        }
    }
}
