﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TestWorkGame
{
    /// <summary>
    /// Логика взаимодействия для IntellectEnterData.xaml
    /// </summary>
    public partial class IntellectEnterData : Window
    {
        private readonly string _selectednumber;
        private readonly string _guessnumber;
        private int _calculatedNumberofBulls;
        private int _calculatedNumberofCows;
        public int NumberofBulls { get; set; }
        public int NumberofCows { get; set; }
        public IntellectEnterData(string selectednumber,string guessnumber)
        {
            InitializeComponent();
            IntellectYourNumberTextBox.Text += selectednumber;
            IntellectGuessNumberTextBox.Text += guessnumber;
            CalculateGuessNumberTextBox.Text += guessnumber;
            _selectednumber = selectednumber;
            _guessnumber = guessnumber;
        }

        private void IntellectNextStepButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((Convert.ToInt32(IntellectEnterNumberofBullsTextBox.Text) >= 0) &&
                    (Convert.ToInt32(IntellectEnterNumberofBullsTextBox.Text) <= 4)
                    && (Convert.ToInt32(IntellectEnterNumberofCowsTextBox.Text) >= 0) &&
                    (Convert.ToInt32(IntellectEnterNumberofCowsTextBox.Text) <= 4))
                {
                    NumberofBulls = Convert.ToInt32(IntellectEnterNumberofBullsTextBox.Text);
                    NumberofCows = Convert.ToInt32(IntellectEnterNumberofCowsTextBox.Text);
                    CalculateNumberofBullsAndCows(out _calculatedNumberofBulls, out _calculatedNumberofCows,
                        _guessnumber);
                    if ((_calculatedNumberofBulls == NumberofBulls) && (_calculatedNumberofCows == NumberofCows))
                    {
                        Close();
                    }
                    else
                        MessageBox.Show(
                            "Вы неправильно ввели данные о числе быков и коров! Проверьте себя спомощью калькулятора!",
                            "Ошибка", MessageBoxButton.OK);
                }
                else throw new ArgumentException("Число быков и коров должно быть строго от 0 до 4!");
            }
            //обрабатываем возникшие исключения
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButton.OK);
            }
            catch (FormatException)
            {
                MessageBox.Show("Число быков и коров не должно содержать никаких символов,кроме цифр!");
            }

        } 
        //вычисление числа быков и коров
        private void CalculateNumberofBullsAndCows(out int numberOfBulls, out int numberOfCows,string guessnumber)
        {
            numberOfBulls = 0;
            numberOfCows = 0;
            //сравниваем каждую цифру одного числа с каждой цифрой другого числа
            //если какие-то совпали,то условие: если и позиция совпала, то это бык, иначе корова
            for (int i = 0; i < _selectednumber.Count(); i++)
            {
                for (int j = 0; j < guessnumber.Count(); j++)
                {
                    if (_selectednumber[i] == guessnumber[j])
                    {
                        if (i == j)
                        {
                            numberOfBulls++;
                        }
                        else numberOfCows++;
                    }
                }
            }
        }

        private void CalculateButton_Click(object sender, RoutedEventArgs e)
        {
            CalculatenumberofCowsTextBox.Clear();
            CalculatenumberofBullsTextBox.Clear();
            CalculateNumberofBullsAndCows(out _calculatedNumberofBulls, out _calculatedNumberofCows,
                CalculateGuessNumberTextBox.Text);
            CalculatenumberofBullsTextBox.Text += _calculatedNumberofBulls;
            CalculatenumberofCowsTextBox.Text += _calculatedNumberofCows;
        }
    }
}
